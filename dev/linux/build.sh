RUN="docker run \
    -v $(pwd):/fabrpy-boilerplate \
    -w /fabrpy-boilerplate \
    node:18-alpine3.15 \
    sh -c "

$RUN "yarn set version berry"
$RUN "YARN_ENABLE_IMMUTABLE_INSTALLS=false yarn install"
$RUN "yarn plugin import workspace-tools"
$RUN "yarn ci:pkg:build"
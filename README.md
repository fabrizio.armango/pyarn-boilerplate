# pyarn-boilerplate
Publish Python code as Yarn Package using GitLab CI/CD and Gitlab Package Registry.


### Local build
``` sh
./dev/linux/build.sh
```

### How to install
- Create a new yarn/npm project
- Setup your registry (in this case: `echo \"@fabrizio.armango:registry\" \"https://gitlab.com/api/v4/projects/38171284/packages/npm/\" >> .yarnrc`)
- Install the package with `yarn add <scope>/package-name` (for instance: ` yarn add @fabrizio.armango/pyarn-simple`)

### Usage
in a node file:
``` es6
import { routineName } from '@fabrizio.armango/pyarn-simple'
routineName();
```
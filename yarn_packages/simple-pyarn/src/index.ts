import { PythonShell, Options as PythonShellOptions, PythonShellError } from 'python-shell';

export function routineName(options?: PythonShellOptions, callback?: (err?: PythonShellError, output?: any[]) => any) {
    PythonShell.run(__dirname + '/main.py', options, callback);
}
